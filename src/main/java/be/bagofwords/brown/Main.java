package be.bagofwords.brown;

import be.bagofwords.ui.UI;

import java.io.IOException;

/**
 * Created by Koen Deschacht (koendeschacht@gmail.com) on 11/12/14.
 */
/*
 * 跟输出的不一样，可能原因：
 * 1：使用的是另一种改进了的算法
 * 2：程序错误，但是不是你修改的地方，这个更加麻烦还得改所有的内容；
 */
public class Main {

//    public static void main(String[] args) throws IOException {
//        String inputFile = "/home/koen/input_news_small.txt";
//        String outputFile = "/home/koen/brown_output6.txt";
//        int minFrequencyOfPhrase = 10;
//        int maxNumberOfClusters = 1000;
//        boolean onlySwapMostFrequentWords = true;
//        long start = System.currentTimeMillis();
//        new BrownClustering(inputFile, outputFile, minFrequencyOfPhrase, maxNumberOfClusters, onlySwapMostFrequentWords).run();
//        long end = System.currentTimeMillis();
//        UI.write("Took " + (end - start) + " ms.");
//    }
    public static void main(String[] args) throws IOException {
        String inputFile = "D:\\zzz_yyhtest\\BrownClusterTest\\example_input.txt";
//        String inputFile = "D:\\zzz_yyhtest\\BrownClusterTest\\input.txt";
        String outputFile = "D:\\zzz_yyhtest\\BrownClusterTest\\output.txt";
        int minFrequencyOfPhrase = 0;
        int maxNumberOfClusters = 50;
        boolean onlySwapMostFrequentWords = false;
        long start = System.currentTimeMillis();
        new BrownClustering(inputFile, outputFile, minFrequencyOfPhrase, maxNumberOfClusters, onlySwapMostFrequentWords).run();
        long end = System.currentTimeMillis();
        UI.write("Took " + (end - start) + " ms.");
    }
}
